const arrowLeft = document.querySelector('#arrow-left');
const arrowRight = document.querySelector('#arrow-right');
const imagesBox = document.querySelector('.carousel-box__images');

let x = 1;

// right arrow
arrowRight.addEventListener('mousedown', (e) => {
    // left click
    if (e.buttons === 1) {
        // left click + ctrl
        if (e.ctrlKey) {
            if (x >= 7) {
                imagesBox.style.marginLeft = '300px';
                x = 1;
            }
            if (x < 7) {
                let oldMarginLeft = px_remover(imagesBox.style.marginLeft);
                imagesBox.style.marginLeft = `${oldMarginLeft - 300}px`;
                x++;
            }
        }
        // just left click 
        else {
            if (x >= 10) {
                imagesBox.style.marginLeft = '150px';
                x = 1;
            }
            if (x < 10) {
                let oldMarginLeft = px_remover(imagesBox.style.marginLeft);
                imagesBox.style.marginLeft = `${oldMarginLeft - 150}px`;
                x++;
            }
        }
    }
    // middle click 
    else if (e.buttons === 4) {
        imagesBox.style.marginLeft = '-1250px';
    }
});

arrowLeft.addEventListener('mousedown', (e) => {
    if (e.buttons === 1) {
        if (e.ctrlKey) {
            if (x > 1) {
                let oldMarginLeft = px_remover(imagesBox.style.marginLeft);
                console.log(imagesBox.style.marginLeft);
                if (imagesBox.style.marginLeft === '300px') {
                    console.log(oldMarginLeft);
                    imagesBox.style.marginLeft = '0px';
                }
                imagesBox.style.marginLeft = `${oldMarginLeft + 300}px`;
            }
        } else {
            if (x > 1) {
                let oldMarginLeft = px_remover(imagesBox.style.marginLeft);
                imagesBox.style.marginLeft = `${oldMarginLeft + 150}px`;
                if (oldMarginLeft === 150) {
                    imagesBox.style.marginLeft = '0px';
                }
            } else {
                imagesBox.style.marginLeft = '0px';
            }
        }
    } else if (e.buttons === 4) {
        imagesBox.style.marginLeft = '0px';
    }
});

function px_remover(margin) {
    var len = margin.length;
    var cut = margin.substr(0, len - 2);
    var to_number = Number(cut);
    return to_number;
}